#include <iostream>
#include <vector>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/checked_delete.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class X
{
    int v_;
public:
    X(int v) : v_(v) { cout << "X(" << v_ << ")" << endl; }

    ~X() { cout << "~X(" << v_ << ")" << endl; }

    int value() const
    {
        return v_;
    }
};


int main()
{
    {
        vector<X*> vecX;

        vecX.push_back(new X(1));
        vecX.push_back(new X(2));

        *(vecX[0]) = X(3);

//        for(vector<X>::iterator it = vecX.begin(); it != vecX.end(); ++it)
//            delete *it;

        for_each(vecX.begin(), vecX.end(), &boost::checked_delete<X>);
    }

    cout << "\n\n";

    {
        boost::ptr_vector<X> vecX(10);

        cout << "size: " << vecX.size() << endl;

        vecX.push_back(new X(1));
        vecX.push_back(new X(2));

        cout << vecX[0].value() << endl;

        vecX[0] = X(1);
        vecX.push_back(new X(3));

        for(boost::ptr_vector<X>::iterator it = vecX.begin(); it != vecX.end(); ++it)
            cout << it->value() << " ";
        cout << endl;
    }
}

