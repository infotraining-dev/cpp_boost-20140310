#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>

class ClassWithType
{
public:
	typedef int type;
};

void some_func(int i)
{
	std::cout << "void some_func(" << i << ")\n";
}

class ExtraClass
{
};

template <typename T>
struct is_extra : public boost::false_type {};

template <>
struct is_extra<ExtraClass> : public  boost::true_type {};


template <typename T>
void some_func(T t, typename boost::disable_if<boost::is_integral<T> >::type* = 0,
		typename boost::disable_if<is_extra<T> >::type* = 0)
{
	typename T::type variable_of_nested_type;
	std::cout << "template <typename T> void some_func(T t)\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<boost::is_unsigned<T> >::type* = 0)
{
	std::cout << "template <typename T> void some_func(T t) : ograniczenie T jest unsigned\n";
}

template <typename T>
void some_func(T t, typename boost::enable_if<is_extra<T> >::type* = 0)
{
	std::cout << "template <typename T> void some_func(T t) : ograniczenie T jest typu ExtraClass\n";
}

int main()
{
	int i = 12;
	short s = 12;
	unsigned int ui = 12;
	unsigned short us = 12;
	ClassWithType cwt;
	ExtraClass ec;

	some_func(i);
	some_func(s);
	some_func(cwt);
	some_func(ui);
	some_func(us);
	some_func(ec);
}
