#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

void some_func(int x)
{
    cout << "some_func(int " << x << ")" << endl;
}

//class MyType
//{
//    int value_;
//public:
//    typedef void my_nested_type;
//    MyType(int v) : value_(v) {}
//    int value() const
//    {
//        return value_;
//    }
//};

//ostream& operator<<(ostream& out, const MyType& mt)
//{
//    out << mt.value();
//    return out;
//}

////template <typename T>
////void some_func(T arg, typename T::my_nested_type* ptr = 0)
////{
////    cout << "some_func(T " << arg << ")" << endl;
////}

//template <typename T>
//typename T::my_nested_type some_func(T arg)
//{
//    cout << "some_func(T " << arg << ")" << endl;
//}

template <typename T>
typename boost::disable_if<boost::is_integral<T> >::type some_func(T arg)
{
    cout << "some_func(T " << arg << ")" << endl;
}

int main()
{
    some_func(10);

    double d = 3.14;
    some_func(d);

    short sx = 4;
    some_func(sx);

//    MyType mt(10);

//    some_func(mt);
}

