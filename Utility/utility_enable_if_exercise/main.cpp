#include <iostream>
#include <iterator>
#include <list>
#include <boost/type_traits/is_pod.hpp>
#include <boost/type_traits/is_float.hpp>
#include <boost/utility/enable_if.hpp>
#include <cstring>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last) do kontenera rozpoczynającego się od dest
// TODO
template <typename InIt, typename OutIt>
void mcopy(InIt start, InIt end, OutIt dest)
{
    cout << "Generic version of mcopy" << endl;

    while(start != end)
    {
        *(dest++) = *(start++);
    }
}

// 2 - napisz zoptymalizowaną wersję mcopy wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
template <typename T>
typename boost::enable_if<boost::is_pod<T> >::type mcopy(T* start, T* end, T* dest)
{
    cout << "Optimized version of mcopy" << endl;
    memcpy(dest, start, (end - start) * sizeof(T));
}

template <typename T, typename Enabled = void>
class DataProcessor
{
public:
    void process_data(T* data, size_t size)
    {
        cout << "DP<T>::process_data()" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{
public:
    void process_data(T* data, size_t size)
    {
        cout << "DP<double>::process_data()" << endl;
    }
};

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    int data1[255];

    DataProcessor<int> dp_int;
    dp_int.process_data(data1, 255);


    double data2[255];
    DataProcessor<double> dp_double;
    dp_double.process_data(data2, 255);
}

