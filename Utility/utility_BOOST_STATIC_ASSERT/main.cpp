#include <iostream>
#include <string>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/shared_ptr.hpp>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
	BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
    BOOST_STATIC_ASSERT(sizeof(int) == 4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));

    std::cout << "Only for sp works!" << std::endl;
}


template <typename T>
struct is_shared_ptr : public boost::false_type
{
};

template <typename T>
struct is_shared_ptr<boost::shared_ptr<T> > : public boost::true_type
{
};

template <typename T>
void only_for_shared_pointers(T ptr)
{
    //BOOST_STATIC_ASSERT(is_shared_ptr<T>::value);
    static_assert(is_shared_ptr<T>::value, "ptr is not sp");

}

int main()
{
    OnlyCompatibleWithIntegralTypes<int> test1;

    int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
    works_with_base_and_derived(arg);

    int* ptr = new int(5);
    boost::shared_ptr<int> sptr(new int(5));

    only_for_shared_pointers(ptr);
}
