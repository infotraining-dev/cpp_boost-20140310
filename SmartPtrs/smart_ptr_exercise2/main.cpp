#include <iostream>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjątki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}
};

class Broker 
{
public:
    Broker(int devno1, int devno2)
        : dev1_(boost::make_shared<Device>(devno1)),
          dev2_(boost::make_shared<Device>(devno2))
    {
	}

private:
    boost::shared_ptr<Device> dev1_;
    boost::shared_ptr<Device> dev2_;
};

int main()
{
	try
	{
		Broker b(1, 2);
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}
}
