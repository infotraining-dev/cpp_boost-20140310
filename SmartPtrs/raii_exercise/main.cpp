#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <memory>
#include <cassert>
#include <vector>
#include <utility>
#include <boost/shared_ptr.hpp>

class FileGuard : boost::noncopyable
{
public:
    FileGuard(FILE* file) : file_(file)
    {
        if (file == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        std::cout << "~FileGuard" << std::endl;
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }

private:
    FILE* file_;
};

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}


// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}


class FileCloser
{
public:
    void operator()(FILE* f)
    {
        fclose(f);
    }
};

// TODO: with shared_ptr
void save_to_file_with_sp(const char* file_name)
{
    boost::shared_ptr<FILE> file(fopen(file_name, "w"), &fclose);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}


class X
{
public:
    X() : buffer_(new char[255])
    {
        std::cout << "X()" << std::endl;
    }

    X(X&& source) : buffer_(source.buffer_)
    {
        source.buffer_ = nullptr;
    }

    ~X()
    {
        std::cout << "~X()" << std::endl;
        delete [] buffer_;
    }

    virtual void do_sth()
    {
        std::cout << "X::do_sth()\n";
    }

private:
    char* buffer_;
};

class DerivedX : public X
{
public:
DerivedX() { std::cout << "DerivedX" << std::endl; }

    ~DerivedX() { std::cout << "~DerivedX()" << std::endl; }

    void do_sth()
    {
        std::cout << "DerivedX::do_sth()" << std::endl;
    }

    void special()
    {
        std::cout << "DerivedX::special()" << std::endl;
    }
};

std::unique_ptr<X> create_x()
{
    std::unique_ptr<X> local(new X());

    return local;
}

int main()
try
{
    {
        std::cout << "+++++\n";

        boost::shared_ptr<X> ptrX;

        ptrX = boost::shared_ptr<new DerivedX()>;

        ptrX->do_sth();

//        boost::shared_ptr<DerivedX> ptrDX = boost::dynamic_pointer_cast<DerivedX>(ptrX);

//        if (ptrDX)
//            ptrDX->special();
    }

    std::cout << "+++++\n";

    {
        std::auto_ptr<X> ptr1_x(new X());

        ptr1_x->do_sth();

        std::auto_ptr<X> ptr2_x = ptr1_x;

        assert(ptr1_x.get() == 0);

        ptr2_x->do_sth();
    }

    {
        std::unique_ptr<X> ptr1_x = create_x(); // unique_ptr<X>(unique_ptr<X>&&)

        ptr1_x->do_sth();

        std::unique_ptr<X> ptr2_x = std::move(ptr1_x);

        assert(ptr1_x.get() == 0);

        ptr2_x->do_sth();

        X x;

        std::vector<X> vec;

        vec.push_back(X());

        std::vector<std::unique_ptr<X>> vec_uptrs;

        vec_uptrs.push_back(std::unique_ptr<X>(new X()));

    }


    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_to_file_with_sp("text.txt");
}
catch(const std::exception& e)
{
    std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
