#include <iostream>
#include <boost/intrusive_ptr.hpp>
#include <cstdlib>

class IUnknown
{
public:
    IUnknown() : ref_count_(0)
    {
    }

    void AddRef()
    {
        ++ref_count_;
    }

    int Release()
    {
        return --ref_count_;
    }

    virtual ~IUnknown() {}
private:
    int ref_count_;
};

class COMObject : public IUnknown
{
public:
    COMObject(int id) :id_(id)
    {
        std::cout << "Ctor COM object - id: " << id_ << std::endl;
    }

    ~COMObject()
    {
        std::cout << "Destructor of COM object - id: " << id_ << std::endl;
    }
private:
    int id_;
};

// TODO - zdefiniować funkcje pomocnicze dla wskaznika intrusive_ptr przechowujacego COMObject
void intrusive_ptr_add_ref(IUnknown* obj)
{
    std::cout << "intrusive_ptr_add_ref(IUnknown* obj)" << std::endl;
    obj->AddRef();
}

void intrusive_ptr_add_ref(COMObject* obj)
{
    std::cout << "intrusive_ptr_add_ref(COMObject* obj)" << std::endl;
    obj->AddRef();
}

void intrusive_ptr_release(IUnknown* obj)
{
    if (obj->Release() == 0)
        delete obj;
}

void intrusive_ptr_release(COMObject* obj)
{
    if (obj->Release() == 0)
        delete obj;
}


boost::intrusive_ptr<COMObject> com_factory(int id)
{
    return boost::intrusive_ptr<COMObject>(new COMObject(id));
}

int main()
{
    boost::intrusive_ptr<IUnknown> outer_ptr;

	std::cout << "Przed wejsciem do zasiegu" << std::endl;
	{
        boost::intrusive_ptr<IUnknown> p1 = com_factory(1);

        {
            boost::intrusive_ptr<IUnknown> p2 = p1;
            outer_ptr = p2;
		}
	}

	std::cout << "Po wyjsciu z zasiegu" << std::endl;
}
