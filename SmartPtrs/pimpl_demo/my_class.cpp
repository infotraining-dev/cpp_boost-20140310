#include "my_class.hpp"

#include <algorithm>

using namespace std;

class MyClass::Impl
{
private:
    int* buffer_;
    size_t size_;
public:
    Impl(size_t size) : buffer_(new int[size]), size_(size)
    {
    }

    Impl(const Impl& source) : buffer_(new int[source.size_]), size_(source.size_)
    {
        copy(source.buffer_, source.buffer_ + size_, buffer_);
    }

    ~Impl()
    {
        delete [] buffer_;
    }

    size_t size() const
    {
        return size_;
    }

    void clear()
    {
        fill(buffer_, buffer_ + size_, 0);
    }

    int& operator[](size_t index)
    {
        return buffer_[index];
    }
};

MyClass::MyClass(size_t size_of_buffer) : impl_(new Impl(size_of_buffer))
{
    impl_->clear();
}

MyClass::MyClass(const MyClass &source) : impl_(new Impl(*(source.impl_)))
{
}

MyClass::~MyClass()
{
}

void MyClass::print()
{
    for(size_t i = 0; i < impl_->size(); ++i)
        cout << (*impl_)[i] << " ";
    cout << endl;
}
