#ifndef MY_CLASS_HPP
#define MY_CLASS_HPP

#include <boost/scoped_ptr.hpp>

class MyClass
{
public:
    MyClass(size_t size_of_buffer);
    MyClass(const MyClass& source);
    ~MyClass();
    void print();
private:
    class Impl;
    boost::scoped_ptr<Impl> impl_;
};

#endif

