#include <memory>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

class X
{
public:
	// konstruktor
	X(int value = 0)
		: value_(value)
	{
		std::cout << "Konstruktor X(" << value_ << ")\n"; 
	}
	
	// destruktor
	~X() 
	{
		std::cout << "Destruktor ~X(" << value_ << ")\n"; 
	}
	
    int value() const
	{
		return value_;
	}

    void set_value(int value)
	{
        value_ = value;
	}

    void unsafe()
    {
        throw std::runtime_error("ERROR");
    }

private:
	int value_;
};

void legacy_code(X* ptr)
{
    ptr->set_value(5);
    cout << "legacy_code: new value = " << ptr->value() << endl;
}

boost::shared_ptr<X> factory(int arg) // TODO: poprawa z wykorzystaniem smart_ptr
{
    return boost::make_shared<X>(arg);
}

X* legacy_function(unsigned int size)
{
    X* xarray = new X[size];

    for(unsigned int i = 0; i < size; ++i)
        xarray[i].set_value(i);

    return xarray;
}

void unsafe1()  // TODO: poprawa z wykorzystaniem smart_ptr
{
    boost::shared_ptr<X> ptrX = factory(4);

	/* kod korzystajacy z ptrX */

    legacy_code(ptrX.get());

    ptrX->unsafe();
}

void unsafe2()
{
    int size = 10;

    boost::scoped_array<X> buffer(legacy_function(size));

    /* kod korzystający z buffer */

    for(int i = 0; i < size; ++i)
        buffer[0].unsafe();
}

int may_throw()
{
    throw std::runtime_error("Error");
}

void foo(boost::shared_ptr<X> arg1, int arg2)
{

}

int main() try
{  
    unsafe1();
    //unsafe2();
}
catch(...)
{
    std::cout << "Zlapalem wyjatek!" << std::endl;
}
