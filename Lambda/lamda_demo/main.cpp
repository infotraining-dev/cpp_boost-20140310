#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>

using namespace std;
using namespace boost::lambda;

template <typename Container>
void print(const Container& c)
{
    std::cout << "[ ";

    for_each(c.begin(), c.end(), std::cout << _1 << " ");

    std::cout << "]\n";
}

int sequence_gen()
{
    static int counter = 0;

    return counter++;
}

int main()
{
    (cout << _1 << _2 << _3 << "\n")("To", "jest", "lambda.");

    cout << "------------------------------------------------\n";

    list<int> numbers(10);

    for_each(numbers.begin(), numbers.end(), _1 = 13);
    print(numbers);

    cout << "------------------------------------------------\n";
    numbers.clear();
    numbers.resize(10);

    for_each(numbers.begin(), numbers.end(), _1 = bind(&sequence_gen));
    print(numbers);

    cout << "------------------------------------------------\n";

    numbers.sort(_1 > _2);
    print(numbers);

    cout << "------------------------------------------------\n";
    cout << "wektor wskaźników:\n";

    vector<int*> pointers(10);

    transform(numbers.begin(), numbers.end(), pointers.begin(), &_1);

    random_shuffle(pointers.begin(), pointers.end());
    print(pointers);

    cout << "po dereferencji: ";
    for_each(pointers.begin(), pointers.end(), cout << *_1 << " ");
    cout << "\n";

    sort(pointers.begin(), pointers.end(), *_1 < *_2);

    cout << "posortowane: ";
    for_each(pointers.begin(), pointers.end(), cout << *_1 << " ");
        cout << "\n";

    cout << "------------------------------------------------\n";
    cout << "liczby nieparzyste:\n";

    remove_copy_if(numbers.begin(),
                   numbers.end(),
                   ostream_iterator<int>(cout, " "),
                   not1(bind2nd(modulus<int>(), 2)));
    cout << "\n";

    // to samo z lambda
    remove_copy_if(numbers.begin(),
                   numbers.end(),
                   ostream_iterator<int>(cout, " "),
                   !(_1 % 2));
    cout << "\n";

    cout << "-----------------------------------------------\n";

    // zastosowanie szablonu funkcji var
    int i = 0;
    for_each(numbers.begin(), numbers.end(), _1 = ( var(i) += 2 ));

    print(numbers);
}
