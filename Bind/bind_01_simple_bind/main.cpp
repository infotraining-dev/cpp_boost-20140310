#include <iostream>
#include <boost/bind.hpp>

void foo(int x, int y)
{
	std::cout << "x: " << x << " y: " << y << std::endl;
}

void nine_arguments(int i1, int i2, int i3, int i4, int i5,
		int i6, int i7, int i8, int i9)
{
	std::cout << i1 << i2 << i3 << i4 << i5 << i6 << i7 << i8 << i9 << std::endl;
}

class Worker
{
	int id_;
public:
	Worker(int id = 0) : id_(id)
	{
	}

	void print(const std::string& prefix) const
	{
		std::cout << prefix << id_ << std::endl;
	}
};

int main()
{
	// proste wiazanie
	boost::bind(&foo, _1, _2)(1, 2);
	boost::bind(&foo, _2, _1)(1, 2);

	std::cout << "------------\n";

	// zamiast std::bind1st lub std::bind2nd
	std::bind1st(std::ptr_fun(&foo), 5)(1);
	// to samo, ale z bind
	boost::bind(&foo, 5, _1)(1);

	std::cout << "------------\n";

	std::bind2nd(std::ptr_fun(&foo), 5)(1);
	boost::bind(&foo, _1, 5)(1);

	std::cout << "------------\n";

	// bind moze wiazac funkcje przyjmujace do 9 argumentow
	int i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, i6 = 6, i7 = 7, i8 = 8, i9 = 9;
	
	(boost::bind(&nine_arguments, _9, _2, _1, _3, _4, _5, _6, _7, _8))(i1, i2, i3, i4, i5, i6, i7, i8, i9);

	std::cout << "------------\n";

    // wiazanie wywolania metody z obiektem
	Worker w(13);

	boost::bind(&Worker::print, _1, "id = ")(w);
}
