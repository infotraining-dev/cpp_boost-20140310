#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <numeric>

using namespace std;

template <typename InIt, typename Func>
void my_for_each(InIt start, InIt end, Func f)
{
    for(InIt it = start; it != end; ++it)
        f(*it);
}

class BindPrint
{
public:
    void operator()(const Person& p)
    {
        p.print();
    }
};

class LambdaC03 : public unary_function<bool, Person>
{
public:
    bool operator()(const Person& p) const
    {
        return p.salary() <= 3000;
    }
};

void my_printer(const Person& p)
{
    p.print();
}

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    vector<Person*> employees_ptrs;
    for(auto it = employees.begin(); it != employees.end(); ++it)
        employees_ptrs.push_back(&(*it));

    cout << "Wszyscy pracownicy:\n";
    boost::function<void (const Person&)> printer = boost::bind(&Person::print, _1);
    printer = my_printer;

    printer(employees[0]);

    //copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    for_each(employees.begin(), employees.end(),
             printer);

    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
//    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
//                   boost::bind(&Person::salary, _1) <= 3000);

    // C++11
    double threshold = 3000.0;

    boost::function<bool (const Person& p)> check_salary = [=](const Person& p) { return p.salary() <= threshold; };

    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   check_salary);


    double sum = 0.0;

    for_each(employees.begin(), employees.end(),
             [=, &sum](const Person& p) { sum += p.salary(); });

    cout << "sum: " << sum << endl;

//    employees.erase(remove_if(employees.begin(), employees.end(), boost::bind(&Person::salary, _1) < 3000),
//                    employees.end());

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";
    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   boost::bind(&Person::age, _1) >= 30);

    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    sort(employees.begin(), employees.end(),
         boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));

    //copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    for_each(employees.begin(), employees.end(), boost::bind(&Person::print, _1));

    // wyświetl kobiety
    cout << "\nKobiety:\n";
    remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"),
                   boost::bind(&Person::gender, _1) == Male);

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";

    double avg = accumulate(employees.begin(), employees.end(), 0,
                            boost::bind(std::plus<double>(), _1, boost::bind(&Person::salary, _2))) / employees.size();

    cout << avg << endl;
}
