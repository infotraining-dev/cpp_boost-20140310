#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/assign.hpp>

using namespace std;

void foo1(int x, double dx)
{
    cout << "foo1(x = " << x << ", dx = " << dx << ")\n";
}

string foo2(int x, double dx, short sx)
{
    return boost::lexical_cast<string>(x) + "; " +
           boost::lexical_cast<string>(dx) + "; " +
           boost::lexical_cast<string>(sx);
}

struct Functor2
{
    typedef string result_type;

    string operator()(int x, double dx, short sx)
    {
        return boost::lexical_cast<string>(x) + "; " +
               boost::lexical_cast<string>(dx) + "; " +
               boost::lexical_cast<string>(sx);
    }
};

void increment(int& x, int value)
{
    x += value;
}

//class Func
//{
//    int x_;
//public:
//    Func() : x_(10)
//    {}

//    void operator()(double dx)
//    {
//        foo1(x_, dx);
//    }
//};

class Person
{
    int id_;
    int age_;
    string name_;
public:
    Person(int id, int age, const string& name) : id_(id), age_(age), name_(name) {}

    void print(const string& prefix) const
    {
        cout << prefix << id_ << " " << name_ << endl;
    }

    int age() const
    {
        return age_;
    }

    bool is_retired() const
    {
        return age_ > 67;
    }
};

int main()
{
    boost::function<void (double)> f1 = boost::bind(&foo1, 10, _1);

    double pi = 3.14;

    f1(pi);  // foo1(10, 3.14)

    auto f2 = boost::bind(&foo2, _2, pi, _1);

    string str = f2(10, 20); // foo2(20, pi, 10)

    cout << "str: " << str << endl;

    auto F2 = boost::bind(Functor2(), _1, pi, _2);

    str = F2(15, 30);

    cout << "str: " << str << endl;

    int x = 0;

    auto f3 = boost::bind(&increment, boost::ref(x), _1);

    f3(10);

    cout << "x = " << x << endl;

    auto f4 = boost::bind(&increment, _1, 20);

    f4(x);

    cout << "x = " << x << endl;

    Person p1(1, 34, "Kowalski");

    auto printer = boost::bind(&Person::print, boost::ref(p1), "Person: ");

    printer();

    using namespace boost::assign;

    list<Person> people;
    people += Person(1, 55,"Kowalski"), Person(2, 69, "Nowak"), Person(3, 7,"Nijaki");

    for(list<Person>::const_iterator it = people.begin(); it != people.end(); ++it)
        it->print("Osoba");


    for_each(people.begin(), people.end(), boost::bind(&Person::print, _1, "Osoba: "));

    int number_of_retired = count_if(people.begin(), people.end(), boost::bind(&Person::is_retired, _1));

    int numbers_of_employee = count_if(people.begin(), people.end(),
                                       boost::bind(&Person::age, _1) > 16 && boost::bind(&Person::age, _1) < 67);

    cout << "number_of_retired: " << number_of_retired << endl;
    cout << "numbers_of_emplyee: " << numbers_of_employee << endl;

    int teenagers = count_if(people.begin(), people.end(), boost::bind(&Person::age, _1) < 18);

    cout << "teenagers: " << teenagers << endl;
}

