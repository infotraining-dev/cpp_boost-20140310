#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>

using namespace std;

class Base
{
public:
	virtual ~Base() {}
	virtual string test() const
	{
		return "Base::test()";
	}
};

class Derived : public Base
{
public:
	virtual string test() const
	{
		return "Derived::test()";
	}
};

int main()
{
    Derived d;

    boost::tuple<int, string, Derived*> t1(-5, "Krotka1", &d);
    boost::tuple<unsigned int, string, Base*> t2;

	t2 = t1;

	cout << "t2.get<0>() = " << t2.get<0>() << "\n"
		 << "t2.get<1>() = " << t2.get<1>() << "\n"
         << "t2.get<2>().test() = " << t2.get<2>()->test() << "\n";
}
