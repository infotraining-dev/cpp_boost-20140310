#include <iostream>
#include <string>
#include <vector>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <boost/assign.hpp>

using namespace std;

template <typename T>
void print_tuple(T& t, string prefix)
{
	cout << boost::tuples::set_open('(')
		 << boost::tuples::set_close(')')
		 << boost::tuples::set_delimiter(',')
		 << prefix << " = " << t << endl;
}

boost::tuple<int, int> find_min_max(const vector<int>& data)
{
    int min_value = *min_element(data.begin(), data.end());
    int max_value = *max_element(data.begin(), data.end());
    return boost::make_tuple(min_value, max_value);
}

int main()
{
	// krotka
	boost::tuple<int, double, string> triple(43, 3.1415, "Krotka...");

	// odwolania do krotki
	cout << "triple = ("
		 << triple.get<0>() << ", "
		 << triple.get<1>() << ", "
         << boost::tuples::get<2>(triple) << ")" << endl;


    // domyslna inicjalizacja krotki
	boost::tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
	triple = boost::make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

	triple.get<2>() = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

	boost::tuple<int&, string&> ref_tpl(x, str);
	ref_tpl.get<0>()++;
	boost::to_upper(ref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    boost::tuple<const int&, string&> cref_tpl = boost::make_tuple(boost::cref(x), boost::ref(str));

	//cref_tpl.get<0>()++; // Blad! Referencja do stalej!
	boost::to_lower(cref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    using namespace boost::assign;
    vector<int> numbers;
    numbers += 1, 5, 7, 3, -1, 10, 13, 9, 20;

    int min;
    int max;

//    boost::tuple<int&, int&> temp(min, max);
//    temp = find_min_max(numbers);
    boost::tie(min, max) = find_min_max(numbers);

    cout << "min = " << min << endl;
    //cout << "max = " << max << endl;
}
