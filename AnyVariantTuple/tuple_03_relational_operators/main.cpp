#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>
#include "PersonalInfo.hpp"

using namespace std;

struct Data
{
    int x, y, z;
};

bool operator==(const Data& d1, const Data& d2)
{
    return boost::tie(d1.x, d1.y, d1.z) == boost::tie(d2.x, d2.y, d2.z);
}

bool operator<(const Data& d1, const Data& d2)
{
    return boost::tie(d1.x, d1.y, d1.z) < boost::tie(d2.x, d2.y, d2.z);
}

int main()
{
    Data d1 { 1, 2, 3 };
    Data d2 { 1, 2, 3 };
    Data d3 { 3, 2, 1 };

    cout << "d1 == d2: " << (d1 == d2) << endl;
    cout << "d1 == d3: " << (d1 == d3) << endl;

	boost::tuple<string, int, PersonalInfo> t1("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));
	boost::tuple<string, int, PersonalInfo> t2("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));
	boost::tuple<string, int, PersonalInfo> t3("different", 2, PersonalInfo("Nikodem", "Dyzma", 28));

	cout.setf(ios::boolalpha);
	cout << t1 << " == " << t2 << ": "  << (t1 == t2) << endl;
	cout << t1 << " == " << t3 << ": "  << (t1 == t3) << endl;

	typedef boost::tuple<string, int, PersonalInfo> tuple_3;
	vector<tuple_3> vec_of_tuples;

	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("5_five", 1, PersonalInfo("Nikodem", "Anonim", 22)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Anonim", 28)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 2, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("4_four", 1, PersonalInfo("Nikodem", "Dyzma", 28)));

	sort(vec_of_tuples.begin(), vec_of_tuples.end());

	cout << "Posortowane krotki:\n";
	copy(vec_of_tuples.begin(),
			vec_of_tuples.end(),
			ostream_iterator<tuple_3>(cout, "\n"));
	cout << "\n" << endl;
}
