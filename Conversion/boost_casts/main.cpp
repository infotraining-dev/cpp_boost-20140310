#define NDEBUG

#include <iostream>
#include <stdexcept>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>


using namespace std;

class Vehicle
{
public:
    virtual ~Vehicle() {}
    virtual void drive() { std::cout << "Vehicle::drive" << std::endl; }
};

class Car : public Vehicle
{
public:
    virtual void drive() { std::cout << "Car::drive" << std::endl; }
    void check_engine() { std::cout << "Car::check_engine" << std::endl; }
};

class Motocycle : public Vehicle
{
public:
    virtual void drive() { std::cout << "Motocycle::drive" << std::endl; }
};

Vehicle* create_vehicle(string id)
{
    if (id == "Car")
        return new Car();
    else if (id == "Motocycle")
        return new Motocycle();

    throw std::runtime_error("Bad vehicle id");
}

void repair_car(Vehicle* v)
{
    Car* car = boost::polymorphic_cast<Car*>(v);

    car->check_engine();
}

int main()
{
    try
    {
        string str = "42.3";

        float d = boost::lexical_cast<float>(str);

        str = boost::lexical_cast<string>(13);

        cout << "d: " << d << "; str: " << str << endl;

        unsigned int x = 4000000000;

        float sx = boost::numeric_cast<float>(x);

        cout << "sx: " << sx << endl;


        Car* c = boost::polymorphic_downcast<Car*>(create_vehicle("Car"));

        c->check_engine();

        Vehicle* v = new Motocycle();

        repair_car(v);
    }
    catch(const std::bad_cast& e)
    {
        cout << e.what() << endl;
    }
}

